#include<stdio.h>
int is_prime(unsigned int x);
int main()
{
	int n,result;
	printf("Enter a positive integer: ");
	scanf("%d",&n);
	result=is_prime(n);
	if(result==1)
		printf("%d is a prime number",n);
	else
		printf("%d is not a prime number",n);
	return 0;
}
int is_prime(unsigned int x)
{
	int i,flag=0;
	for(i=2;i<=x/2;i++)
	{
		if(x%i==0)
			flag=1;
	}
	if(flag==1)
		return 0;
	else
		return 1;
}